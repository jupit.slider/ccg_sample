--
-- Database: `camp`
--
CREATE DATABASE IF NOT EXISTS `camp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `camp`;


-- drop table ec_item_master;
-- drop table ec_item_stock;
-- drop table ec_cart;
-- drop table ec_user;

-- --------------------------------------------------------

--
-- テーブルの構造 `ec_item_master`
--

CREATE TABLE IF NOT EXISTS `ec_item_master` (
  `item_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `ec_item_stock`
--

CREATE TABLE IF NOT EXISTS `ec_item_stock` (
  `item_id` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルの構造 `ec_cart`
--

CREATE TABLE IF NOT EXISTS `ec_cart` (
  `cart_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `ec_user`
--

CREATE TABLE IF NOT EXISTS `ec_user` (
  `user_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL UNIQUE,
  `password` varchar(255) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
