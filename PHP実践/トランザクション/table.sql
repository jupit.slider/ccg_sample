CREATE TABLE `test_drink_stock` (
  `drink_id` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL
);

ALTER TABLE `test_drink_stock`
 ADD PRIMARY KEY (`drink_id`);
 