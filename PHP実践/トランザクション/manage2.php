<?php
// MySQL接続情報
$host     = 'localhost';
$username = 'root';   // MySQLのユーザ�
$password = 'root';   // MySQLのパスワー�
$dbname   = 'camp';   // MySQLのDB�
// MySQL用のDNS�字�
$dns = 'mysql:dbname='.$dbname.';host='.$host;

$img_dir    = './img/';

$sql_kind   = '';
$result_msg = '';
$data       = array();
$err_msg    = array();

if (isset($_POST['sql_kind']) === TRUE) {
  $sql_kind = $_POST['sql_kind'];
}

if ($sql_kind === 'insert') {

  $new_name   = '';
  $new_price  = '';
  $new_stock  = '';
  $new_img    = 'no_image.png';

  if (isset($_POST['new_name']) === TRUE) {
    $new_name = preg_replace('/\A[�\s]*|[�\s]*\z/u', '', $_POST['new_name']);
  }

  if (isset($_POST['new_price']) === TRUE) {
    $new_price = preg_replace('/\A[�\s]*|[�\s]*\z/u', '', $_POST['new_price']);
  }

  if (isset($_POST['new_stock']) === TRUE) {
    $new_stock = preg_replace('/\A[�\s]*|[�\s]*\z/u', '', $_POST['new_stock']);
  }

  //  HTTP POST でファイルがア�プロードされたか確�
  if (is_uploaded_file($_FILES['new_img']['tmp_name']) === TRUE) {

    $new_img = $_FILES['new_img']['name'];

    // 画像�拡張子取�
    $extension = pathinfo($new_img, PATHINFO_EXTENSION);

    // 拡張子チェ�ク
    if ($extension === 'jpg' || $extension == 'jpeg' || $extension == 'png') {

      // ユニ�クID生�し保存ファイルの名前を変更
      $new_img = md5(uniqid(mt_rand(), true)) . '.' . $extension;

      // 同名ファイルが存在するか確�
      if (is_file($img_dir . $new_img) !== TRUE) {

        // ファイルを移動し保�
        if (move_uploaded_file($_FILES['new_img']['tmp_name'], $img_dir . $new_img) !== TRUE) {
          $err_msg[] = 'ファイルア�プロードに失敗しました';
        }

      // 生�したIDがかぶることは通常な�ため、IDの再生成ではなく�ア�プロードを�すよ�にした
      } else {
        $err_msg[] = 'ファイルア�プロードに失敗しました。�度お試しく�さい�';
      }

    } else {
      $err_msg[] = 'ファイル形式が異なります�画像ファイルはJPEG又�PNGのみ利用可能です�';
    }

  } else {
    $err_msg[] = 'ファイルを選択してください';
  }

} else if ($sql_kind === 'update') {

  $update_stock = '';
  $drink_id     = '';

  if (isset($_POST['update_stock']) === TRUE) {
    $update_stock = preg_replace('/\A[�\s]*|[�\s]*\z/u', '', $_POST['update_stock']);
  }

  if (isset($_POST['drink_id']) === TRUE) {
    $drink_id = $_POST['drink_id'];
  }
}

try {
  // �ータベ�スに接�
  $dbh = new PDO($dns, $username, $password);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  if (count($err_msg) === 0 && $_SERVER['REQUEST_METHOD'] === 'POST') {

    if ($sql_kind === 'insert') {

      // 現在日時を取�
      $now_date = date('Y-m-d H:i:s');

      // トランザクション開�
      $dbh->beginTransaction();

      try {
        // SQL�を作�
        $sql = 'INSERT INTO test_drink_master (drink_name, price, img, create_datetime) VALUES (?, ?, ?, ?)';
        // SQL�を実行する準備
        $stmt = $dbh->prepare($sql);
        // SQL�のプレースホル�に値をバイン�
        $stmt->bindValue(1, $new_name,    PDO::PARAM_STR);
        $stmt->bindValue(2, $new_price,   PDO::PARAM_INT);
        $stmt->bindValue(3, $new_img,     PDO::PARAM_STR);
        $stmt->bindValue(4, $now_date,    PDO::PARAM_STR);

        // SQLを実�
        $stmt->execute();

        // INSERTされたデータのIDを取�
        $drink_id = $dbh->lastInsertId('drink_id');

        // SQL�を作�
        $sql = 'INSERT INTO test_drink_stock (drink_id, stock, create_datetime) VALUES (?, ?, ?)';
        // SQL�を実行する準備
        $stmt = $dbh->prepare($sql);
        // SQL�のプレースホル�に値をバイン�
        $stmt->bindValue(1, $drink_id,    PDO::PARAM_INT);
        $stmt->bindValue(2, $new_stock,   PDO::PARAM_STR);
        $stmt->bindValue(3, $now_date,    PDO::PARAM_STR);
        // SQLを実�
        $stmt->execute();
        // コミッ�
        $dbh->commit();
        // 表示メ�セージの設�
        $result_msg =  '追�成功';
      } catch (PDOException $e) {
        // ロールバック処�
        $dbh->rollback();
        // 例外をスロー
        throw $e;
      }

    } else if ($sql_kind === 'update') {

      try {
        // SQL�を作�
        $sql = 'UPDATE test_drink_stock SET stock = ?, update_datetime = ? WHERE drink_id = ?';
        // SQL�を実行する準備
        $stmt = $dbh->prepare($sql);
        // SQL�のプレースホル�に値をバイン�
        $stmt->bindValue(1, $update_stock,    PDO::PARAM_INT);
        $stmt->bindValue(2, $update_datetime, PDO::PARAM_STR);
        $stmt->bindValue(3, $drink_id,        PDO::PARAM_INT);
        // SQLを実�
        $stmt->execute();
        // 表示メ�セージの設�
        $result_msg = '在庫変更成功';
      } catch (PDOException $e) {
        // 例外をスロー
        throw $e;
      }
    }
  }

  try {
    // SQL�を作�
    $sql = 'SELECT 
          test_drink_master.drink_id,
          test_drink_master.drink_name,
          test_drink_master.price,
          test_drink_master.img,
          test_drink_stock.stock
        FROM test_drink_master JOIN test_drink_stock
        ON  test_drink_master.drink_id = test_drink_stock.drink_id';
    // SQL�を実行する準備
    $stmt = $dbh->prepare($sql);
    // SQLを実�
    $stmt->execute();
    // レコード�取�
    $rows = $stmt->fetchAll();
    // 1行ずつ結果を��で取得しま�
    $i = 0;
    foreach ($rows as $row) {
      $data[$i]['drink_id']   = htmlspecialchars($row['drink_id'],   ENT_QUOTES, 'UTF-8');
      $data[$i]['drink_name'] = htmlspecialchars($row['drink_name'], ENT_QUOTES, 'UTF-8');
      $data[$i]['price']      = htmlspecialchars($row['price'],      ENT_QUOTES, 'UTF-8');
      $data[$i]['img']        = htmlspecialchars($row['img'],        ENT_QUOTES, 'UTF-8');
      $data[$i]['stock']      = htmlspecialchars($row['stock'],      ENT_QUOTES, 'UTF-8');
      $i++;
    }

  } catch (PDOException $e) {
    // 例外をスロー
    throw $e;
  }
} catch (PDOException $e) {
  $err_msg[] = '予期せぬエラーが発生しました。管��へお問�合わせく�さい�'.$e->getMessage();
}

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>自動販売�</title>
  <style>
    section {
      margin-bottom: 20px;
      border-top: solid 1px;
    }

    table {
      width: 660px;
      border-collapse: collapse;
    }

    table, tr, th, td {
      border: solid 1px;
      padding: 10px;
      text-align: center;
    }

    caption {
      text-align: left;
    }

    .text_align_right {
      text-align: right;
    }

    .drink_name_width {
      width: 100px;
    }

    .input_text_width {
      width: 60px;
    }
  </style>
</head>
<body>
<?php if (empty($result_msg) !== TRUE) { ?>
  <p><?php print $result_msg; ?></p>
<?php } ?>
<?php foreach ($err_msg as $value) { ?>
  <p><?php print $value; ?></p>
<?php } ?>
  <h1>自動販売機管��ール</h1>
  <section>
    <h2>新規商品追�</h2>
    <form method="post" enctype="multipart/form-data">
      <div><label>名前: <input type="text" name="new_name" value=""></label></div>
      <div><label>値段: <input type="text" name="new_price" value=""></label></div>
      <div><label>個数: <input type="text" name="new_stock" value=""></label></div>
      <div><input type="file" name="new_img"></div>
      <input type="hidden" name="sql_kind" value="insert">
      <div><input type="submit" value="�品を追�"></div>
    </form>
  </section>
  <section>
    <h2>�品情報変更</h2>
    <table>
      <caption>�品�覧</caption>
      <tr>
        <th>�品画�</th>
        <th>�品名</th>
        <th>価格</th>
        <th>在庫数</th>
      </tr>
<?php foreach ($data as $value)  { ?>
      <tr>
        <form method="post">
          <td><img src="<?php print $img_dir . $value['img']; ?>"></td>
          <td class="drink_name_width"><?php print $value['drink_name']; ?></td>
          <td class="text_align_right"><?php print $value['price']; ?>�</td>
          <td><input type="text"  class="input_text_width text_align_right" name="update_stock" value="<?php print $value['stock']; ?>">�&nbsp;&nbsp;<input type="submit" value="変更"></td>
          <input type="hidden" name="drink_id" value="<?php print $value['drink_id']; ?>">
          <input type="hidden" name="sql_kind" value="update">
        </form>
      <tr>
<?php } ?>
    </table>
  </section>
</body>
</html>