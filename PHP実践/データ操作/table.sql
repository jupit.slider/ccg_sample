CREATE TABLE `test_drink_master` (
  `drink_id` int(11) NOT NULL,
  `drink_name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `create_datetime` datetime NOT NULL
);

ALTER TABLE `test_drink_master`
 ADD PRIMARY KEY (`drink_id`);

ALTER TABLE `test_drink_master`
MODIFY `drink_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

