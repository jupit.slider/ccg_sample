<?php
//
// ユーザIDをチェック
//
$userid = $_POST['userid'];
$pattern = '/^[a-zA-Z0-9]{6,8}+$/';
if( preg_match($pattern, $userid) ) {
  print($userid." ：ユーザIDは正しい形式で入力されています。<br>");
} else {
  print($userid." ：ユーザIDは正しくない形式で入力されています。<br>");
}

//
// 年齢をチェック
//
$age = $_POST['age'];
$pattern = '/^[0-9]+$/';
if( preg_match($pattern, $age) ) {
  print($age." ：正しい年齢の形式です。<br>");
} else {
  print($age." ：正しくない年齢の形式です。<br>");
}

//
// メールアドレスをチェック
//
$email = $_POST['email'];
$pattern = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/iD';
if( preg_match($pattern, $email) ) {
  print($email." ：正しいメールアドレスの形式です。<br>");
} else {
  print($email." ：正しくないメールアドレスの形式です。<br>");
}

//
// 電話番号をチェック
//
$tel = $_POST['tel'];
$pattern = '/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}/';
if( preg_match($pattern, $tel) ) {
  print($tel." ：正しい電話番号の形式です。<br>");
} else {
  print($tel." ：正しくない電話番号の形式です。<br>");
}
?>
